#!/usr/bin/env bash

GIT_REPO_ID=nomad-sigex-php-sdk
GIT_USER_ID=perfect-systems

echo "[~] Download openapi-generator-cli from official site"
wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/7.2.0/openapi-generator-cli-7.2.0.jar -O openapi-generator-cli.jar

echo "[~] Deleted previous sdk"
rm -rf .openapi-generator docs lib test

echo "[~] Start generation PHP API client"
java -jar openapi-generator-cli.jar generate --skip-validate-spec \
 --additional-properties invokerPackage=PerfectSystems\\Nomad\\Sigex\\Api \
 -i ./schema.json -g php \
 --git-user-id ${GIT_USER_ID} \
 --git-repo-id ${GIT_REPO_ID}

#rm ./openapi-generator-cli.jar

echo "[~] Remember to add to .gitignore .idea and schema.json"

echo "[~] Prepare files and commit update"
git add .
git commit -m "Refresh API to fresh version of swagger documentation"
