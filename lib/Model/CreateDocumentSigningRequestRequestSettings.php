<?php
/**
 * CreateDocumentSigningRequestRequestSettings
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  PerfectSystems\Nomad\Sigex\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * ЭЦП через QR на базе eGov mobile
 *
 * For more information, please visit [ and [
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.2.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace PerfectSystems\Nomad\Sigex\Api\Model;

use \ArrayAccess;
use \PerfectSystems\Nomad\Sigex\Api\ObjectSerializer;

/**
 * CreateDocumentSigningRequestRequestSettings Class Doc Comment
 *
 * @category Class
 * @package  PerfectSystems\Nomad\Sigex\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class CreateDocumentSigningRequestRequestSettings implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'createDocumentSigningRequest_request_settings';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'private' => 'bool',
        'signatures_limit' => 'int',
        'force_archive' => 'bool',
        'switch_to_private_after_limit_reached' => 'bool',
        'unique' => 'string[]',
        'strict_signers_requirements' => 'bool',
        'signers_requirements' => '\PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'private' => null,
        'signatures_limit' => null,
        'force_archive' => null,
        'switch_to_private_after_limit_reached' => null,
        'unique' => null,
        'strict_signers_requirements' => null,
        'signers_requirements' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'private' => false,
        'signatures_limit' => false,
        'force_archive' => false,
        'switch_to_private_after_limit_reached' => false,
        'unique' => false,
        'strict_signers_requirements' => false,
        'signers_requirements' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'private' => 'private',
        'signatures_limit' => 'signaturesLimit',
        'force_archive' => 'forceArchive',
        'switch_to_private_after_limit_reached' => 'switchToPrivateAfterLimitReached',
        'unique' => 'unique',
        'strict_signers_requirements' => 'strictSignersRequirements',
        'signers_requirements' => 'signersRequirements'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'private' => 'setPrivate',
        'signatures_limit' => 'setSignaturesLimit',
        'force_archive' => 'setForceArchive',
        'switch_to_private_after_limit_reached' => 'setSwitchToPrivateAfterLimitReached',
        'unique' => 'setUnique',
        'strict_signers_requirements' => 'setStrictSignersRequirements',
        'signers_requirements' => 'setSignersRequirements'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'private' => 'getPrivate',
        'signatures_limit' => 'getSignaturesLimit',
        'force_archive' => 'getForceArchive',
        'switch_to_private_after_limit_reached' => 'getSwitchToPrivateAfterLimitReached',
        'unique' => 'getUnique',
        'strict_signers_requirements' => 'getStrictSignersRequirements',
        'signers_requirements' => 'getSignersRequirements'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('private', $data ?? [], null);
        $this->setIfExists('signatures_limit', $data ?? [], null);
        $this->setIfExists('force_archive', $data ?? [], null);
        $this->setIfExists('switch_to_private_after_limit_reached', $data ?? [], null);
        $this->setIfExists('unique', $data ?? [], null);
        $this->setIfExists('strict_signers_requirements', $data ?? [], null);
        $this->setIfExists('signers_requirements', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets private
     *
     * @return bool|null
     */
    public function getPrivate()
    {
        return $this->container['private'];
    }

    /**
     * Sets private
     *
     * @param bool|null $private private
     *
     * @return self
     */
    public function setPrivate($private)
    {
        if (is_null($private)) {
            throw new \InvalidArgumentException('non-nullable private cannot be null');
        }
        $this->container['private'] = $private;

        return $this;
    }

    /**
     * Gets signatures_limit
     *
     * @return int|null
     */
    public function getSignaturesLimit()
    {
        return $this->container['signatures_limit'];
    }

    /**
     * Sets signatures_limit
     *
     * @param int|null $signatures_limit signatures_limit
     *
     * @return self
     */
    public function setSignaturesLimit($signatures_limit)
    {
        if (is_null($signatures_limit)) {
            throw new \InvalidArgumentException('non-nullable signatures_limit cannot be null');
        }
        $this->container['signatures_limit'] = $signatures_limit;

        return $this;
    }

    /**
     * Gets force_archive
     *
     * @return bool|null
     */
    public function getForceArchive()
    {
        return $this->container['force_archive'];
    }

    /**
     * Sets force_archive
     *
     * @param bool|null $force_archive force_archive
     *
     * @return self
     */
    public function setForceArchive($force_archive)
    {
        if (is_null($force_archive)) {
            throw new \InvalidArgumentException('non-nullable force_archive cannot be null');
        }
        $this->container['force_archive'] = $force_archive;

        return $this;
    }

    /**
     * Gets switch_to_private_after_limit_reached
     *
     * @return bool|null
     */
    public function getSwitchToPrivateAfterLimitReached()
    {
        return $this->container['switch_to_private_after_limit_reached'];
    }

    /**
     * Sets switch_to_private_after_limit_reached
     *
     * @param bool|null $switch_to_private_after_limit_reached switch_to_private_after_limit_reached
     *
     * @return self
     */
    public function setSwitchToPrivateAfterLimitReached($switch_to_private_after_limit_reached)
    {
        if (is_null($switch_to_private_after_limit_reached)) {
            throw new \InvalidArgumentException('non-nullable switch_to_private_after_limit_reached cannot be null');
        }
        $this->container['switch_to_private_after_limit_reached'] = $switch_to_private_after_limit_reached;

        return $this;
    }

    /**
     * Gets unique
     *
     * @return string[]|null
     */
    public function getUnique()
    {
        return $this->container['unique'];
    }

    /**
     * Sets unique
     *
     * @param string[]|null $unique unique
     *
     * @return self
     */
    public function setUnique($unique)
    {
        if (is_null($unique)) {
            throw new \InvalidArgumentException('non-nullable unique cannot be null');
        }
        $this->container['unique'] = $unique;

        return $this;
    }

    /**
     * Gets strict_signers_requirements
     *
     * @return bool|null
     */
    public function getStrictSignersRequirements()
    {
        return $this->container['strict_signers_requirements'];
    }

    /**
     * Sets strict_signers_requirements
     *
     * @param bool|null $strict_signers_requirements strict_signers_requirements
     *
     * @return self
     */
    public function setStrictSignersRequirements($strict_signers_requirements)
    {
        if (is_null($strict_signers_requirements)) {
            throw new \InvalidArgumentException('non-nullable strict_signers_requirements cannot be null');
        }
        $this->container['strict_signers_requirements'] = $strict_signers_requirements;

        return $this;
    }

    /**
     * Gets signers_requirements
     *
     * @return \PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner[]|null
     */
    public function getSignersRequirements()
    {
        return $this->container['signers_requirements'];
    }

    /**
     * Sets signers_requirements
     *
     * @param \PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner[]|null $signers_requirements signers_requirements
     *
     * @return self
     */
    public function setSignersRequirements($signers_requirements)
    {
        if (is_null($signers_requirements)) {
            throw new \InvalidArgumentException('non-nullable signers_requirements cannot be null');
        }
        $this->container['signers_requirements'] = $signers_requirements;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


