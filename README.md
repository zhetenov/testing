# OpenAPIClient-php

For more information, please visit [ and [


## Installation & Usage

### Requirements

PHP 7.4 and later.
Should also work with PHP 8.0.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/perfect-systems/nomad-sigex-php-sdk.git"
    }
  ],
  "require": {
    "perfect-systems/nomad-sigex-php-sdk": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');




$apiInstance = new PerfectSystems\Nomad\Sigex\Api\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_document_signing_request_request = new \PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequest(); // \PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequest

try {
    $result = $apiInstance->createDocumentSigningRequest($create_document_signing_request_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->createDocumentSigningRequest: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://sigex.kz/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**createDocumentSigningRequest**](docs/Api/DefaultApi.md#createdocumentsigningrequest) | **POST** / | Create a new document signing request
*DefaultApi* | [**fixValuesOfDocumentHashes**](docs/Api/DefaultApi.md#fixvaluesofdocumenthashes) | **POST** /{documentId}/data | Hash document
*DefaultApi* | [**getDocumentSigningDetails**](docs/Api/DefaultApi.md#getdocumentsigningdetails) | **GET** /{documentId} | Retrieve document signing details
*DefaultApi* | [**getSignatures**](docs/Api/DefaultApi.md#getsignatures) | **GET** /{signURL} | получение подписей
*DefaultApi* | [**getSignedDocument**](docs/Api/DefaultApi.md#getsigneddocument) | **POST** /{documentId}/buildDDC | Get Signed Document
*DefaultApi* | [**registerEgovQrSigningProcedure**](docs/Api/DefaultApi.md#registeregovqrsigningprocedure) | **POST** /egovQr | зарегистрировать новую процедуру подписания ЭЦП через QR
*DefaultApi* | [**sendDataForSigning**](docs/Api/DefaultApi.md#senddataforsigning) | **POST** /{dataURL} | отправка данных на подписание

## Models

- [CreateDocumentSigningRequest200Response](docs/Model/CreateDocumentSigningRequest200Response.md)
- [CreateDocumentSigningRequestRequest](docs/Model/CreateDocumentSigningRequestRequest.md)
- [CreateDocumentSigningRequestRequestEmailNotifications](docs/Model/CreateDocumentSigningRequestRequestEmailNotifications.md)
- [CreateDocumentSigningRequestRequestSettings](docs/Model/CreateDocumentSigningRequestRequestSettings.md)
- [CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner](docs/Model/CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner.md)
- [FixValuesOfDocumentHashes200Response](docs/Model/FixValuesOfDocumentHashes200Response.md)
- [FixValuesOfDocumentHashes200ResponseDigests](docs/Model/FixValuesOfDocumentHashes200ResponseDigests.md)
- [FixValuesOfDocumentHashes200ResponseEmailNotifications](docs/Model/FixValuesOfDocumentHashes200ResponseEmailNotifications.md)
- [GetDocumentSigningDetails200Response](docs/Model/GetDocumentSigningDetails200Response.md)
- [GetDocumentSigningDetails200ResponseSettings](docs/Model/GetDocumentSigningDetails200ResponseSettings.md)
- [GetDocumentSigningDetails200ResponseSignaturesInner](docs/Model/GetDocumentSigningDetails200ResponseSignaturesInner.md)
- [GetDocumentSigningDetails200ResponseSignaturesInnerOcsp](docs/Model/GetDocumentSigningDetails200ResponseSignaturesInnerOcsp.md)
- [GetDocumentSigningDetails200ResponseSignaturesInnerTsp](docs/Model/GetDocumentSigningDetails200ResponseSignaturesInnerTsp.md)
- [GetDocumentSigningDetails200ResponseSignaturesInnerTspSubjectStructureInnerInner](docs/Model/GetDocumentSigningDetails200ResponseSignaturesInnerTspSubjectStructureInnerInner.md)
- [GetSignedDocument200Response](docs/Model/GetSignedDocument200Response.md)
- [RegisterEgovQrSigningProcedure200Response](docs/Model/RegisterEgovQrSigningProcedure200Response.md)
- [RegisterEgovQrSigningProcedureRequest](docs/Model/RegisterEgovQrSigningProcedureRequest.md)
- [SendDataForSigningRequest](docs/Model/SendDataForSigningRequest.md)
- [SendDataForSigningRequestDocumentsToSignInner](docs/Model/SendDataForSigningRequestDocumentsToSignInner.md)
- [SendDataForSigningRequestDocumentsToSignInnerDocument](docs/Model/SendDataForSigningRequestDocumentsToSignInnerDocument.md)

## Authorization
Endpoints do not require authorization.

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
