# PerfectSystems\Nomad\Sigex\Api\DefaultApi

All URIs are relative to https://sigex.kz/api, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**createDocumentSigningRequest()**](DefaultApi.md#createDocumentSigningRequest) | **POST** / | Create a new document signing request |
| [**fixValuesOfDocumentHashes()**](DefaultApi.md#fixValuesOfDocumentHashes) | **POST** /{documentId}/data | Hash document |
| [**getDocumentSigningDetails()**](DefaultApi.md#getDocumentSigningDetails) | **GET** /{documentId} | Retrieve document signing details |
| [**getSignatures()**](DefaultApi.md#getSignatures) | **GET** /{signURL} | получение подписей |
| [**getSignedDocument()**](DefaultApi.md#getSignedDocument) | **POST** /{documentId}/buildDDC | Get Signed Document |
| [**registerEgovQrSigningProcedure()**](DefaultApi.md#registerEgovQrSigningProcedure) | **POST** /egovQr | зарегистрировать новую процедуру подписания ЭЦП через QR |
| [**sendDataForSigning()**](DefaultApi.md#sendDataForSigning) | **POST** /{dataURL} | отправка данных на подписание |


## `createDocumentSigningRequest()`

```php
createDocumentSigningRequest($create_document_signing_request_request): \PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequest200Response
```

Create a new document signing request

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PerfectSystems\Nomad\Sigex\Api\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_document_signing_request_request = new \PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequest(); // \PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequest

try {
    $result = $apiInstance->createDocumentSigningRequest($create_document_signing_request_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->createDocumentSigningRequest: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **create_document_signing_request_request** | [**\PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequest**](../Model/CreateDocumentSigningRequestRequest.md)|  | |

### Return type

[**\PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequest200Response**](../Model/CreateDocumentSigningRequest200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `fixValuesOfDocumentHashes()`

```php
fixValuesOfDocumentHashes($document_id, $body): \PerfectSystems\Nomad\Sigex\Api\Model\FixValuesOfDocumentHashes200Response
```

Hash document

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PerfectSystems\Nomad\Sigex\Api\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$document_id = 'document_id_example'; // string
$body = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->fixValuesOfDocumentHashes($document_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->fixValuesOfDocumentHashes: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **document_id** | **string**|  | |
| **body** | **\SplFileObject****\SplFileObject**|  | |

### Return type

[**\PerfectSystems\Nomad\Sigex\Api\Model\FixValuesOfDocumentHashes200Response**](../Model/FixValuesOfDocumentHashes200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/octet-stream`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getDocumentSigningDetails()`

```php
getDocumentSigningDetails($document_id): \PerfectSystems\Nomad\Sigex\Api\Model\GetDocumentSigningDetails200Response
```

Retrieve document signing details

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PerfectSystems\Nomad\Sigex\Api\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$document_id = 'document_id_example'; // string

try {
    $result = $apiInstance->getDocumentSigningDetails($document_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getDocumentSigningDetails: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **document_id** | **string**|  | |

### Return type

[**\PerfectSystems\Nomad\Sigex\Api\Model\GetDocumentSigningDetails200Response**](../Model/GetDocumentSigningDetails200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getSignatures()`

```php
getSignatures($sign_url)
```

получение подписей

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PerfectSystems\Nomad\Sigex\Api\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$sign_url = 'sign_url_example'; // string

try {
    $apiInstance->getSignatures($sign_url);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getSignatures: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sign_url** | **string**|  | |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getSignedDocument()`

```php
getSignedDocument($document_id, $file_name, $without_document_visualization, $without_signatures_visualization, $without_qr_codes_in_signatures_visualization, $language, $body): \PerfectSystems\Nomad\Sigex\Api\Model\GetSignedDocument200Response
```

Get Signed Document

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PerfectSystems\Nomad\Sigex\Api\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$document_id = 'document_id_example'; // string
$file_name = 'file_name_example'; // string
$without_document_visualization = True; // bool
$without_signatures_visualization = True; // bool
$without_qr_codes_in_signatures_visualization = True; // bool
$language = 'language_example'; // string
$body = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->getSignedDocument($document_id, $file_name, $without_document_visualization, $without_signatures_visualization, $without_qr_codes_in_signatures_visualization, $language, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getSignedDocument: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **document_id** | **string**|  | |
| **file_name** | **string**|  | |
| **without_document_visualization** | **bool**|  | |
| **without_signatures_visualization** | **bool**|  | |
| **without_qr_codes_in_signatures_visualization** | **bool**|  | |
| **language** | **string**|  | |
| **body** | **\SplFileObject****\SplFileObject**|  | |

### Return type

[**\PerfectSystems\Nomad\Sigex\Api\Model\GetSignedDocument200Response**](../Model/GetSignedDocument200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/octet-stream`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `registerEgovQrSigningProcedure()`

```php
registerEgovQrSigningProcedure($register_egov_qr_signing_procedure_request): \PerfectSystems\Nomad\Sigex\Api\Model\RegisterEgovQrSigningProcedure200Response
```

зарегистрировать новую процедуру подписания ЭЦП через QR

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PerfectSystems\Nomad\Sigex\Api\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$register_egov_qr_signing_procedure_request = new \PerfectSystems\Nomad\Sigex\Api\Model\RegisterEgovQrSigningProcedureRequest(); // \PerfectSystems\Nomad\Sigex\Api\Model\RegisterEgovQrSigningProcedureRequest

try {
    $result = $apiInstance->registerEgovQrSigningProcedure($register_egov_qr_signing_procedure_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->registerEgovQrSigningProcedure: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **register_egov_qr_signing_procedure_request** | [**\PerfectSystems\Nomad\Sigex\Api\Model\RegisterEgovQrSigningProcedureRequest**](../Model/RegisterEgovQrSigningProcedureRequest.md)|  | |

### Return type

[**\PerfectSystems\Nomad\Sigex\Api\Model\RegisterEgovQrSigningProcedure200Response**](../Model/RegisterEgovQrSigningProcedure200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendDataForSigning()`

```php
sendDataForSigning($data_url, $send_data_for_signing_request)
```

отправка данных на подписание

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new PerfectSystems\Nomad\Sigex\Api\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$data_url = 'data_url_example'; // string
$send_data_for_signing_request = new \PerfectSystems\Nomad\Sigex\Api\Model\SendDataForSigningRequest(); // \PerfectSystems\Nomad\Sigex\Api\Model\SendDataForSigningRequest

try {
    $apiInstance->sendDataForSigning($data_url, $send_data_for_signing_request);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->sendDataForSigning: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **data_url** | **string**|  | |
| **send_data_for_signing_request** | [**\PerfectSystems\Nomad\Sigex\Api\Model\SendDataForSigningRequest**](../Model/SendDataForSigningRequest.md)|  | |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
