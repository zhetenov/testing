# # SendDataForSigningRequestDocumentsToSignInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name_ru** | **string** |  | [optional]
**name_kz** | **string** |  | [optional]
**name_en** | **string** |  | [optional]
**document** | [**\PerfectSystems\Nomad\Sigex\Api\Model\SendDataForSigningRequestDocumentsToSignInnerDocument**](SendDataForSigningRequestDocumentsToSignInnerDocument.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
