# # FixValuesOfDocumentHashes200ResponseDigests

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_1_2_398_3_10_1_3_1** | **string** |  | [optional]
**_1_2_398_3_10_1_3_3** | **string** |  | [optional]
**_2_16_840_1_101_3_4_2_1** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
