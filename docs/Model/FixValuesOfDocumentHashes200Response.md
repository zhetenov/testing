# # FixValuesOfDocumentHashes200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_id** | **string** |  | [optional]
**digests** | [**\PerfectSystems\Nomad\Sigex\Api\Model\FixValuesOfDocumentHashes200ResponseDigests**](FixValuesOfDocumentHashes200ResponseDigests.md) |  | [optional]
**signed_data_size** | **int** |  | [optional]
**data_archived** | **bool** |  | [optional]
**email_notifications** | [**\PerfectSystems\Nomad\Sigex\Api\Model\FixValuesOfDocumentHashes200ResponseEmailNotifications**](FixValuesOfDocumentHashes200ResponseEmailNotifications.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
