# # GetDocumentSigningDetails200ResponseSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**private** | **bool** |  | [optional]
**switch_to_private_after_limit_reached** | **bool** |  | [optional]
**strict_signers_requirements** | **bool** |  | [optional]
**signatures_limit** | **int** |  | [optional]
**unique** | **string[]** |  | [optional]
**signers_requirements** | [**\PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner[]**](CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner.md) |  | [optional]
**document_access** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
