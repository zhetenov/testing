# # CreateDocumentSigningRequestRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**sign_type** | **string** |  | [optional]
**signature** | **string** |  | [optional]
**email_notifications** | [**\PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequestEmailNotifications**](CreateDocumentSigningRequestRequestEmailNotifications.md) |  | [optional]
**settings** | [**\PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequestSettings**](CreateDocumentSigningRequestRequestSettings.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
