# # CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iin** | **string** |  | [optional]
**bin** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
