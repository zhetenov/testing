# # CreateDocumentSigningRequestRequestSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**private** | **bool** |  | [optional]
**signatures_limit** | **int** |  | [optional]
**force_archive** | **bool** |  | [optional]
**switch_to_private_after_limit_reached** | **bool** |  | [optional]
**unique** | **string[]** |  | [optional]
**strict_signers_requirements** | **bool** |  | [optional]
**signers_requirements** | [**\PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner[]**](CreateDocumentSigningRequestRequestSettingsSignersRequirementsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
