# # GetDocumentSigningDetails200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**signed_data_size** | **int** |  | [optional]
**data_archived** | **bool** |  | [optional]
**can_be_archived** | **bool** |  | [optional]
**email_notifications** | [**\PerfectSystems\Nomad\Sigex\Api\Model\CreateDocumentSigningRequestRequestEmailNotifications**](CreateDocumentSigningRequestRequestEmailNotifications.md) |  | [optional]
**settings** | [**\PerfectSystems\Nomad\Sigex\Api\Model\GetDocumentSigningDetails200ResponseSettings**](GetDocumentSigningDetails200ResponseSettings.md) |  | [optional]
**signatures_total** | **int** |  | [optional]
**signatures** | [**\PerfectSystems\Nomad\Sigex\Api\Model\GetDocumentSigningDetails200ResponseSignaturesInner[]**](GetDocumentSigningDetails200ResponseSignaturesInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
