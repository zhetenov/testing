# # RegisterEgovQrSigningProcedure200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expire_at** | **int** |  | [optional]
**data_url** | **string** |  | [optional]
**sign_url** | **string** |  | [optional]
**e_gov_mobile_launch_link** | **string** |  | [optional]
**e_gov_business_launch_link** | **string** |  | [optional]
**qr_code** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
