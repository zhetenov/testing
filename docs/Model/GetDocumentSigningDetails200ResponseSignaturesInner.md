# # GetDocumentSigningDetails200ResponseSignaturesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **string** |  | [optional]
**business_id** | **string** |  | [optional]
**sign_algorithm** | **string** |  | [optional]
**stored_at** | **int** |  | [optional]
**sign_id** | **int** |  | [optional]
**sign_type** | **string** |  | [optional]
**tsp** | [**\PerfectSystems\Nomad\Sigex\Api\Model\GetDocumentSigningDetails200ResponseSignaturesInnerTsp**](GetDocumentSigningDetails200ResponseSignaturesInnerTsp.md) |  | [optional]
**ocsp** | [**\PerfectSystems\Nomad\Sigex\Api\Model\GetDocumentSigningDetails200ResponseSignaturesInnerOcsp**](GetDocumentSigningDetails200ResponseSignaturesInnerOcsp.md) |  | [optional]
**cert_sign_algorithm** | **string** |  | [optional]
**serial_number** | **string** |  | [optional]
**from** | **int** |  | [optional]
**until** | **int** |  | [optional]
**subject** | **string** |  | [optional]
**subject_structure** | **\PerfectSystems\Nomad\Sigex\Api\Model\GetDocumentSigningDetails200ResponseSignaturesInnerTspSubjectStructureInnerInner[][]** |  | [optional]
**subject_alt_name** | **string** |  | [optional]
**subject_alt_name_structure** | **object[]** |  | [optional]
**issuer** | **string** |  | [optional]
**issuer_structure** | **\PerfectSystems\Nomad\Sigex\Api\Model\GetDocumentSigningDetails200ResponseSignaturesInnerTspSubjectStructureInnerInner[][]** |  | [optional]
**key_storage** | **string** |  | [optional]
**policy_ids** | **string[]** |  | [optional]
**key_usages** | **string[]** |  | [optional]
**ext_key_usages** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
